PRAGMA foreign_keys=ON;

CREATE TABLE iso_country (
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    alpha2 VARCHAR(2) NOT NULL UNIQUE,
    alpha3 VARCHAR(3) NOT NULL UNIQUE,
    short_name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE event (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    organization_name VARCHAR NOT NULL,
    short_name VARCHAR,
    year INTEGER NOT NULL,
    attendance INTEGER,
    start_month INTEGER,
    start_day INTEGER,
    end_month INTEGER,
    end_day INTEGER,
    location_venue VARCHAR,
    location_city VARCHAR,
    location_division VARCHAR,
    location_country INTEGER,
    location_area VARCHAR,
    FOREIGN KEY (location_country) REFERENCES iso_country (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE creator (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    name VARCHAR NOT NULL,
    short_name VARCHAR,
    country INTEGER,
    FOREIGN KEY (country) REFERENCES iso_country (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE creator_link (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    creator_id INTEGER NOT NULL,
    url VARCHAR NOT NULL,
    FOREIGN KEY (creator_id) REFERENCES creator (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE video (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    url VARCHAR NOT NULL,
    event_id INTEGER NOT NULL,
    song_artist VARCHAR,
    song_title VARCHAR,
    meta_svc VARCHAR,
    meta_vid VARCHAR CHECK (NOT (meta_vid IS NOT NULL AND meta_svc IS NULL)),
    meta_last_checked VARCHAR CHECK ((meta_last_checked IS NULL AND meta_svc IS NULL) OR (meta_last_checked IS NOT NULL AND meta_svc IS NOT NULL)),
    meta_last_checked_result VARCHAR CHECK ((meta_last_checked_result IS NULL AND meta_svc IS NULL) OR (meta_last_checked_result IS NOT NULL AND meta_svc IS NOT NULL)),
    meta_last_updated VARCHAR CHECK (NOT (meta_last_updated IS NOT NULL AND meta_svc IS NULL)),
    meta_title VARCHAR CHECK (NOT (meta_last_updated IS NOT NULL AND meta_svc IS NULL)),
    meta_posted VARCHAR CHECK (NOT (meta_last_updated IS NOT NULL AND meta_svc IS NULL)),
    meta_runtime VARCHAR CHECK (NOT (meta_last_updated IS NOT NULL AND meta_svc IS NULL)),
    FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE creator_video (
    creator_id INTEGER NOT NULL,
    video_id INTEGER NOT NULL,
    role VARCHAR NOT NULL,
    PRIMARY KEY (creator_id, video_id),
    FOREIGN KEY (creator_id) REFERENCES creator (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (video_id) REFERENCES video (id) ON DELETE RESTRICT ON UPDATE CASCADE
);
