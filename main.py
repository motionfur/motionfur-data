#!/usr/bin/env python3

import yaml
import sys

from motionfur.generate_db import generate_db
from motionfur.yt_metadata import update_yt_metadata, update_yt_thumbs

with open('config.yaml', 'r') as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)


def dbfill():
    generate_db(config, 'motionfur.db', 'sql/motionfur_schema.sql')


def ytmetadata():
    update_yt_metadata(config, 'motionfur.db')


def ytthumbs():
    update_yt_thumbs(config, 'motionfur.db')


if __name__ == '__main__':
    globals()[sys.argv[1]]()
