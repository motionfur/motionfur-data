import gspread
import sqlite3

from .dbfill import country, creator, creator_link, creator_video, event, video


def generate_db(config: dict, db_name: str, init_name: str) -> None:
    gc = gspread.service_account(filename=config['auth'])
    document = gc.open_by_key(config['doc'])

    con = sqlite3.connect(db_name)
    con.execute('PRAGMA foreign_keys=ON')

    with open(init_name, 'r') as f:
        init_sql = f.read()
    con.executescript(init_sql)
    con.commit()

    country.fill_data(document.get_worksheet_by_id(config['sheet']['country']), con)
    event.fill_data(document.get_worksheet_by_id(config['sheet']['event']), con)
    creator.fill_data(document.get_worksheet_by_id(config['sheet']['creator']), con)
    creator_link.fill_data(document.get_worksheet_by_id(config['sheet']['link']), con)
    video.fill_data(document.get_worksheet_by_id(config['sheet']['con-video']), con)
    creator_video.fill_data(document.get_worksheet_by_id(config['sheet']['con-video']), con)

    con.commit()
    con.close()
