import traceback

import gspread
import sqlite3


def fill_data(sheet: gspread.worksheet.Worksheet, con: sqlite3.Connection) -> None:
    lines = sheet.get_all_records(
        numericise_ignore=['all'],
        expected_headers=[
            'creator',
            'url',
        ]
    )
    cur = con.cursor()

    for i, line in enumerate(lines, 2):
        try:
            if not line['creator'].strip(): continue  # NOT NULL
            if not line['url'].strip(): continue  # NOT NULL

            field_id = None  # AUTOINCREMENT
            field_creator = line['creator'].strip()
            field_url = line['url'].strip()

            cur.execute("""
INSERT INTO creator_link
VALUES (
    :id,
    (SELECT id from creator WHERE name = :creator),
    :url
)""",
                {
                    'id': field_id,
                    'creator': field_creator,
                    'url': field_url,
                }
            )
        except Exception as ex:
            print(f'Sheet "creator_link", row {i}', end=', ')
            print(traceback.format_exc().splitlines()[-1])
