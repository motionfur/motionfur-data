import traceback

import gspread
import sqlite3


def fill_data(sheet: gspread.worksheet.Worksheet, con: sqlite3.Connection) -> None:
    lines = sheet.get_all_records(
        numericise_ignore=['all'],
        expected_headers=[
            'name',
            'short_name',
            'country_name',
        ]
    )
    cur = con.cursor()

    for i, line in enumerate(lines, 2):
        try:
            if not line['name'].strip(): continue  # NOT NULL

            field_id = None  # AUTOINCREMENT
            field_name = line['name'].strip()
            field_short_name = line['short_name'].strip() or None
            field_country = line['country_name'].strip() or None

            cur.execute("""
INSERT INTO creator
VALUES (
    :id,
    :name,
    :short_name,
    (SELECT id from iso_country WHERE short_name = :country)
)""",
                {
                    'id': field_id,
                    'name': field_name,
                    'short_name': field_short_name,
                    'country': field_country,
                }
            )
        except Exception as ex:
            print(f'Sheet "creator", row {i}', end=', ')
            print(traceback.format_exc().splitlines()[-1])
