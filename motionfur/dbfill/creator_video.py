import traceback

import gspread
import sqlite3


def fill_data(sheet: gspread.worksheet.Worksheet, con: sqlite3.Connection) -> None:
    lines = sheet.get_all_records(
        numericise_ignore=['all'],
        expected_headers=[
            'Filmmaker',
            'URL',
        ]
    )
    cur = con.cursor()

    for i, line in enumerate(lines, 2):
        try:
            if not line['Filmmaker'].strip(): continue  # NOT NULL
            if not line['URL'].strip(): continue  # NOT NULL
            if not line['Furry Event'].strip(): continue  # Detect header rows
            if not line['Year'].strip(): continue  # Detect header rows

            field_creator = line['Filmmaker'].strip()
            field_url = line['URL'].strip()
            field_role = 'Contributor'  # TODO

            for field_creator_single in field_creator.split('\u2795'):
                cur.execute("""
INSERT INTO creator_video
VALUES (
    (SELECT id from creator WHERE name = :creator),
    (SELECT id from video WHERE url = :url),
    :role
)""",
                    {
                        'creator': field_creator_single.strip(),
                        'url': field_url,
                        'role': field_role,
                    }
                )
        except Exception as ex:
            print(f'Sheet "video", row {i}', end=', ')
            print(traceback.format_exc().splitlines()[-1])
