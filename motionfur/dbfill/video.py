import traceback

import gspread
import sqlite3


def fill_data(sheet: gspread.worksheet.Worksheet, con: sqlite3.Connection) -> None:
    lines = sheet.get_all_records(
        numericise_ignore=['all'],
        expected_headers=[
            'Furry Event',
            'Year',
            'URL',
            'Song Artist',
            'Song Title',
        ]
    )
    cur = con.cursor()

    for i, line in enumerate(lines, 2):
        try:
            if not line['URL'].strip(): continue  # NOT NULL
            if not line['Furry Event'].strip(): continue  # NOT NULL
            if not line['Year'].strip(): continue  # NOT NULL

            field_id = None  # AUTOINCREMENT
            field_url = line['URL'].strip()
            field_song_artist = line['Song Artist'].strip() or None
            field_song_title = line['Song Title'].strip() or None

            field_event_name = line['Furry Event'].strip()
            field_event_year = int(line['Year'].strip())

            cur.execute("""
INSERT INTO video (id, url, event_id, song_artist, song_title)
VALUES (
    :id,
    :url,
    (SELECT id from event WHERE organization_name = :event_name AND year = :event_year),
    :song_artist,
    :song_title
)""",
                {
                    'id': field_id,
                    'url': field_url,
                    'event_name': field_event_name,
                    'event_year': field_event_year,
                    'song_artist': field_song_artist,
                    'song_title': field_song_title,
                }
            )
        except Exception as ex:
            print(f'Sheet "video", row {i}', end=', ')
            print(traceback.format_exc().splitlines()[-1])
