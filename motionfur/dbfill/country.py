import traceback

import gspread
import sqlite3


def fill_data(sheet: gspread.worksheet.Worksheet, con: sqlite3.Connection) -> None:
    lines = sheet.get_all_records(
        numericise_ignore=['all'],
        expected_headers=[
            'id',
            'alpha2',
            'alpha3',
            'short_name'
        ]
    )
    cur = con.cursor()

    for i, line in enumerate(lines, 2):
        try:
            if not line['id'].strip(): continue  # NOT NULL
            if not line['alpha2'].strip(): continue  # NOT NULL
            if not line['alpha3'].strip(): continue  # NOT NULL
            if not line['short_name'].strip(): continue  # NOT NULL

            field_id = int(line['id'])
            field_alpha2 = line['alpha2'].strip()
            field_alpha3 = line['alpha3'].strip()
            field_short_name = line['short_name'].strip()

            cur.execute("""
INSERT INTO iso_country
VALUES (
    :id,
    :alpha2,
    :alpha3,
    :short_name
)""",
                {
                    'id': field_id,
                    'alpha2': field_alpha2,
                    'alpha3': field_alpha3,
                    'short_name': field_short_name,
                }
            )
        except Exception as ex:
            print(f'Sheet "iso_country", row {i}', end=', ')
            print(traceback.format_exc().splitlines()[-1])
