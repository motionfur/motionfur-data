import calendar
import traceback

import gspread
import sqlite3


def fill_data(sheet: gspread.worksheet.Worksheet, con: sqlite3.Connection) -> None:
    lines = sheet.get_all_records(
        numericise_ignore=['all'],
        expected_headers=[
            'Organization Name',
            'Year',
            'Start Date',
            'End Date',
            'City',
            'Area',
            'Country',
            'Division',
            'Venue',
            'Attendance',
        ]
    )
    cur = con.cursor()

    for i, line in enumerate(lines, 2):
        try:
            if not line['Organization Name'].strip(): continue  # NOT NULL
            if not line['Year'].strip(): continue  # NOT NULL

            field_id = None  # AUTOINCREMENT
            field_organization_name = line['Organization Name'].strip()
            field_short_name = None  # TODO: not implemented
            field_year = int(line['Year'].strip())

            field_attendance = line['Attendance'].strip() or None
            if field_attendance:
                if field_attendance == '[NP]':
                    field_attendance = -1
                elif field_attendance == '[VV]':
                    field_attendance = -2
                else:
                    field_attendance = int(field_attendance.replace(',', ''))

            temp_start = line['Start Date'].strip().split(' ')
            field_start_month = temp_start[0] or None
            if field_start_month:
                field_start_month = list(calendar.month_name).index(temp_start[0])
            field_start_day = int(temp_start[1]) if len(temp_start) == 2 else None

            temp_end = line['Start Date'].strip().split(' ')
            field_end_month = temp_end[0] or None
            if field_end_month:
                field_end_month = list(calendar.month_name).index(temp_end[0])
            field_end_day = int(temp_end[1]) if len(temp_end) == 2 else None

            field_location_venue = line['Venue'].strip() or None
            field_location_city = line['City'].strip() or None
            field_location_division = line['Division'].strip() or None
            field_location_country = line['Country'].strip() or None
            field_location_area = line['Area'].strip() or None

            cur.execute("""
INSERT INTO event
VALUES (
    :id,
    :organization_name,
    :short_name,
    :year,
    :attendance,
    :start_month,
    :start_day,
    :end_month,
    :end_day,
    :location_venue,
    :location_city,
    :location_division,
    (SELECT id from iso_country WHERE short_name = :location_country),
    :location_area
)""",
                {
                    'id': field_id,
                    'organization_name': field_organization_name,
                    'short_name': field_short_name,
                    'year': field_year,
                    'attendance': field_attendance,
                    'start_month': field_start_month,
                    'start_day': field_start_day,
                    'end_month': field_end_month,
                    'end_day': field_end_day,
                    'location_venue': field_location_venue,
                    'location_city': field_location_city,
                    'location_division': field_location_division,
                    'location_country': field_location_country,
                    'location_area': field_location_area,
                }
            )
        except Exception as ex:
            print(f'Sheet "event", row {i}', end=', ')
            print(traceback.format_exc().splitlines()[-1])
