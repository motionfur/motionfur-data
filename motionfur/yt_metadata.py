import csv
import io
import isodate
import sqlite3
import urllib.request
from datetime import datetime, timedelta, timezone

from google.cloud import storage
from google.oauth2 import service_account

from .ytdata.ytdata import extract_yt_id, yt_api_fetch_metadata

URL = 'https://i.ytimg.com/vi/{}/hqdefault.jpg'

yt_metadata_fields = [
    'id',
    'url',
    'checked',
    'result',
    'updated',
    'title',
    'posted',
    'runtime',
]

yt_thumb_cache_fields = [
    'id',
    'checked',
    'result',
    'updated',
]


def update_yt_metadata(config: dict, db_name: str) -> None:
    con = sqlite3.connect(db_name)
    con.execute('PRAGMA foreign_keys=ON')

    # Attempt to retrieve yt metadata cache, otherwise blank cache template
    credentials = service_account.Credentials.from_service_account_file(config['auth'])
    storage_client = storage.Client(credentials=credentials)
    bucket_data = storage_client.get_bucket(config['bucket-data'])
    blob = bucket_data.get_blob('yt_metadata_cache.csv')
    blob_str = ','.join(yt_metadata_fields) if blob is None else blob.download_as_bytes().decode('utf-8')
    reader = csv.DictReader(io.StringIO(blob_str))
    cached_lookup = {extract_yt_id(row['url']): row for row in reader}

    # Populate new cache with either cached data, or download it using the api
    new_cache = []
    cur = con.cursor()
    cur.execute("SELECT url FROM video")
    for i, *_ in cur.fetchall():
        yt_id = extract_yt_id(i)
        if yt_id is None:  # not a yt video, or id could not be extracted
            print(f'[\u001b[33mSKIP\u001b[0m] {i}')
            continue

        # must meet all the conditions to reuse a cached one
        if yt_id in cached_lookup.keys():
            current_item = cached_lookup[yt_id]
            current_item['url'] = i
            last_updated = isodate.parse_datetime(current_item['updated']).replace(tzinfo=timezone.utc)
            today = datetime.utcnow().replace(tzinfo=timezone.utc)
            if (today - last_updated) <= timedelta(days=30):
                print(f'[\u001b[34mKEEP\u001b[0m] {yt_id}: ', end='')
                print(f'Expires in: {timedelta(days=30) - (today.replace(microsecond=0) - last_updated)}')  # TODO
                new_cache.append(current_item)
                continue
            else:
                print(f'[\u001b[36mSTAL\u001b[0m] {yt_id}:', end=' ')
                new_metadata = yt_api_fetch_metadata(config, yt_id)
                current_item['checked'] = new_metadata['checked']
                current_item['result'] = new_metadata['result']
                if current_item['result'] != 'ok':
                    print(f'[\u001b[31mFAIL\u001b[0m]', end=' ')
                else:
                    print(f'[\u001b[32m OK \u001b[0m]', end=' ')
                    current_item['updated'] = new_metadata['checked']
                    current_item['title'] = new_metadata['title']
                    current_item['posted'] = new_metadata['posted']
                    current_item['runtime'] = new_metadata['runtime']
                new_cache.append(current_item)
                print(f'Expired: {(last_updated.replace(tzinfo=None) + timedelta(days=30))}')
        else:
            print(f'[\u001b[36mMISS\u001b[0m] {yt_id}: ', end='')
            current_item = yt_api_fetch_metadata(config, yt_id)
            current_item['url'] = i
            if current_item['result'] != 'ok':
                print(f'[\u001b[31mFAIL\u001b[0m]')
            else:
                print(f'[\u001b[32m OK \u001b[0m]')
                current_item['updated'] = current_item['checked']
                new_cache.append(current_item)

    with open('yt_metadata_cache.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=yt_metadata_fields, extrasaction='ignore')
        writer.writeheader()
        writer.writerows(new_cache)

    blob = bucket_data.blob('yt_metadata_cache.csv')
    blob.upload_from_filename('yt_metadata_cache.csv')
    print(f'File yt_metadata_cache.csv uploaded.')
    for row in new_cache:
        cur.execute("""
UPDATE video
SET
    meta_svc = :svc,
    meta_vid = :vid,
    meta_last_checked = :last_checked,
    meta_last_checked_result = :last_checked_result,
    meta_last_updated = :last_updated,
    meta_title = :title,
    meta_posted = :posted,
    meta_runtime = :runtime
WHERE
    url = :url
""",
            {
                'svc': 'yt',
                'vid': row['id'],
                'last_checked': row['checked'],
                'last_checked_result': row['result'],
                'last_updated': row['updated'],
                'title': row['title'],
                'posted': row['posted'],
                'runtime': row['runtime'],
                'url': row['url'],
            }
        )

    con.commit()
    cur.close()
    con.close()

def update_yt_thumbs(config: dict, db_name: str) -> None:
    con = sqlite3.connect(db_name)
    con.execute('PRAGMA foreign_keys=ON')

    # Attempt to retrieve yt thumb cache, otherwise blank cache template
    credentials = service_account.Credentials.from_service_account_file(config['auth'])
    storage_client = storage.Client(credentials=credentials)
    bucket_data = storage_client.get_bucket(config['bucket-data'])
    blob = bucket_data.get_blob('yt_thumb_cache.csv')
    blob_str = ','.join(yt_thumb_cache_fields) if blob is None else blob.download_as_bytes().decode('utf-8')
    reader = csv.DictReader(io.StringIO(blob_str))
    cached_lookup = {row['id']: row for row in reader}

    bucket_pub = storage_client.get_bucket(config['bucket-public'])

    new_cache = []
    cur = con.cursor()
    cur.execute("SELECT url FROM video")
    for i, *_ in cur.fetchall():
        yt_id = extract_yt_id(i)
        if yt_id is None:  # not a yt video, or id could not be extracted
            print(f'[\u001b[33mSKIP\u001b[0m] {i}')
            continue
        print(f'Processing {yt_id}...', end=' ')
        if yt_id not in cached_lookup.keys():
            current_item = {
                'id': yt_id,
                'checked': datetime.utcnow().replace(microsecond=0).isoformat(sep='T') + "Z",
            }
            try:
                file, headers = urllib.request.urlretrieve(URL.format(yt_id))
                blob = bucket_pub.blob(f't/{yt_id}.jpg')
                blob.upload_from_filename(file, content_type='image/jpeg')
                print(f'added {yt_id} to bucket')
                current_item['result'] = 'ok'
                current_item['updated'] = current_item['checked']
            except Exception as e:
                print(e)
                current_item['result'] = 'fail'
        else:
            current_item = cached_lookup[yt_id]
            last_updated = isodate.parse_datetime(current_item['updated']).replace(tzinfo=timezone.utc)
            today = datetime.utcnow().replace(tzinfo=timezone.utc)
            if (today - last_updated) > timedelta(days=30):
                try:
                    file, *_ = urllib.request.urlretrieve(URL.format(yt_id))
                    blob = bucket_pub.blob(f't/{yt_id}.jpg')
                    blob.upload_from_filename(file, content_type='image/jpeg')
                    print(f'added {yt_id} to bucket')
                    current_item['result'] = 'ok'
                    current_item['updated'] = current_item['checked']
                except:
                    print(e)
                    current_item['result'] = 'fail'
            else:
                print('')

        if current_item.get('updated'):
            new_cache.append(current_item)

    with open('yt_thumb_cache.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=yt_thumb_cache_fields, extrasaction='ignore')
        writer.writeheader()
        writer.writerows(new_cache)

    blob = bucket_data.blob('yt_thumb_cache.csv')
    blob.upload_from_filename('yt_thumb_cache.csv')
    print(f'File yt_thumb_cache.csv uploaded.')
