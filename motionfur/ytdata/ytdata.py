import isodate
import re
import requests
from datetime import datetime


def extract_yt_id(url: str) -> str:
    match = re.search(r'((?<=(v|V)/)|(?<=be/)|(?<=(\?|\&)v=)|(?<=embed/))([\w-]+)', url)
    return match if match is None else match.group(0)


def yt_api_fetch_metadata(config: dict, id: str) -> dict[str, str]:
    try:
        r = requests.get(config['yt-api'], params={"key": config['yt-key'], "part": "snippet", "id": id})
        r.raise_for_status()
        snippet = r.json()["items"][0]["snippet"]

        r = requests.get(config['yt-api'], params={"key": config['yt-key'], "part": "contentDetails", "id": id})
        r.raise_for_status()
        details = r.json()["items"][0]["contentDetails"]

        return {
            'id': id,
            'checked': datetime.utcnow().replace(microsecond=0).isoformat(sep='T') + "Z",
            'result': 'ok',
            'title': snippet["title"],
            'posted': snippet["publishedAt"],
            'duration': details["duration"],
            'runtime': str(isodate.parse_duration(details["duration"]).seconds),
            'description': snippet["description"],
        }
    except:
        return {
            'id': id,
            'checked': datetime.utcnow().replace(microsecond=0).isoformat(sep='T') + "Z",
            'result': 'fail',
        }
